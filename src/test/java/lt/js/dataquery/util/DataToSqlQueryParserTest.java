package lt.js.dataquery.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class DataToSqlQueryParserTest {

	private final ToSqlQueryParser parser = new DataToSqlQueryParser();

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamEqual() {
		assertEquals("1=1 and (id = '!@#$%^&*()')", parser.convertQueryToSql(
				"EQUAL(id,\"!@#$%^&*()\")"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamDoubleEqual() {
		assertEquals("1=1 and (views = 101) and (content = 'trak@34ČĘĖĮŠ')",
				parser.convertQueryToSql("EQUAL(views,101)EQUAL(content,\"trak@34ČĘĖĮŠ\")"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamAnd() {
		assertEquals("1=1 and ((title = '') and (timestamp = 100))",
				parser.convertQueryToSql("AND(EQUAL(title,\"\"),EQUAL(timestamp,100))"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamOr() {
		assertEquals("1=1 and ((content = '20000000...') or (views = 1))",
				parser.convertQueryToSql("OR(EQUAL(content,\"20000000...\"),EQUAL(views,1))"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamDoubleOr() {
		assertEquals("1=1 and ((views = 20000000) or (id = 'tasa.')) and ((timestamp = 10) or (title = '..'))",
				parser.convertQueryToSql(
						"OR(EQUAL(views,20000000),EQUAL(id,\"tasa.\"))OR(EQUAL(timestamp,10),EQUAL(title,\"..\"))"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamNot() {
		assertEquals("1=1 and ((content<>'asdasdsadasdawqeeqweqw89787879997897879845'))",
				parser.convertQueryToSql("NOT(EQUAL(content,\"asdasdsadasdawqeeqweqw89787879997897879845\"))"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamGreaterThan() {
		assertEquals("1=1 and (title>'0')", parser.convertQueryToSql("GREATER_THAN(title,\"0\")"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamDoubleGreaterThan() {
		assertEquals("1=1 and (title>'babalasd') and (views>100)",
				parser.convertQueryToSql("GREATER_THAN(title,\"babalasd\")GREATER_THAN(views,100)"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamLessThan() {
		assertEquals("1=1 and (timestamp<1000000000000)",
				parser.convertQueryToSql("LESS_THAN(timestamp,1000000000000)"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamDoubleLessThan() {
		assertEquals("1=1 and (timestamp<-1010) and (views<91.8)",
				parser.convertQueryToSql("LESS_THAN(timestamp,-1010)LESS_THAN(views,91.8)"));
	}

	@Test
	public void convertQueryToSql_ReturnsValidSqlWhereClause_IfRequestParamMultipleOperators() {
		assertEquals(
				"1=1 and ((id = 'first-post') or (id = 'second-post')) and (id = 'first-post') "
						+ "and (views = 100) and ((id<>'first-post')) and ((id = 'first-post') and ( views = 100)) "
						+ "and (views<100) and (views>100)",
				parser.convertQueryToSql(
						"OR(EQUAL(id,\"first-post\"),EQUAL(id,\"second-post\"))EQUAL(id,\"first-post\")"
								+ "EQUAL(views,100)NOT(EQUAL(id,\"first-post\"))AND(EQUAL(id,\"first-post\"),"
								+ "EQUAL( views,100))LESS_THAN(views,100)GREATER_THAN(views,100)"));
	}
}
