package lt.js.dataquery.util;

import java.util.Base64;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.error.EmptyValueException;
import lt.js.dataquery.error.MaxValueLengthException;

public class RequestValidatorTest {

	private final RequestValidator validator = new RequestValidator();

	private final String queryParam = formatLongQueryParam();

	@Test
	public void validate_ThrowsEmptyValueException_IfParameterIdIsEmpty() {
		QueryRequest queryRequest = new QueryRequest(null, "My   First   Post", queryParam, 1, 1555832341);
		Assertions.assertThrows(EmptyValueException.class, () -> {
			validator.validate(queryRequest);
		});
	}

	@Test
	public void validate_ThrowsMaxValueLengthException_IfParameterIdLengthIsTooLong() {
		QueryRequest queryRequest = new QueryRequest(queryParam, "My   First   Post", "Hello   World!", 1,
				1555832341);
		Assertions.assertThrows(MaxValueLengthException.class, () -> {
			validator.validate(queryRequest);
		});
	}

	@Test
	public void validate_ThrowsMaxValueLengthException_IfParameterTitleLengthIsTooLong() {
		QueryRequest queryRequest = new QueryRequest("first-post", queryParam, "Hello   World!", 1, 1555832341);
		Assertions.assertThrows(MaxValueLengthException.class, () -> {
			validator.validate(queryRequest);
		});
	}

	@Test
	public void validate_ThrowsMaxValueLengthException_IfParameterContentLengthIsTooLong() {
		QueryRequest queryRequest = new QueryRequest("1", "1010", queryParam, 1, 1555832341);
		Assertions.assertThrows(MaxValueLengthException.class, () -> {
			validator.validate(queryRequest);
		});
	}

	@Test
	public void validate_ThrowsEmptyValueException_IfParameterQueryIsEmpty() {
		Assertions.assertThrows(EmptyValueException.class, () -> {
			validator.validate("   ");
		});
	}

	private String formatLongQueryParam() {
		byte[] bytes = new byte[2100];
		return Base64.getEncoder().encodeToString(bytes);
	}

}
