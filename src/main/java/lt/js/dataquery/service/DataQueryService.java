package lt.js.dataquery.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.dao.DataQueryRepository;
import lt.js.dataquery.entity.DataQuery;
import lt.js.dataquery.util.RequestValidator;
import lt.js.dataquery.util.ToSqlQueryParser;

@Service
public class DataQueryService {

	private DataQueryRepository dataQueryRepository;

	private RequestValidator validator;

	private ToSqlQueryParser parser;

	@Autowired
	public DataQueryService(DataQueryRepository dataQueryRepository, RequestValidator validator,
			ToSqlQueryParser parser) {
		this.dataQueryRepository = dataQueryRepository;
		this.validator = validator;
		this.parser = parser;
	}

	public List<DataQuery> getDataQuery(String query) {
		validator.validate(query);
		String sqlQuery = parser.convertQueryToSql(query);
		return dataQueryRepository.findByWhereClause(sqlQuery);
	}

	public void postDataQuery(QueryRequest request) {
		validator.validate(request);
		saveToDb(request.convertToDataQuery());
	}

	private void saveToDb(DataQuery dataQuery) {
		if (dataQueryRepository.findById(dataQuery.getId()).isEmpty())
			dataQueryRepository.save(dataQuery);
		else
			dataQueryRepository.update(dataQuery.getTitle(), dataQuery.getContent(), dataQuery.getViews(),
					dataQuery.getTimestamp(), dataQuery.getId());
	}

}
