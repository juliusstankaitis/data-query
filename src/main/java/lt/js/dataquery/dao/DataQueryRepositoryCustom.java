package lt.js.dataquery.dao;

import java.util.List;

import lt.js.dataquery.entity.DataQuery;

public interface DataQueryRepositoryCustom {

	List<DataQuery> findByWhereClause(String whereClause);
}
