package lt.js.dataquery.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import lt.js.dataquery.bean.QueryRequest;
import lt.js.dataquery.error.EmptyValueException;
import lt.js.dataquery.error.MaxValueLengthException;

@Component
public class RequestValidator {

	public void validate(QueryRequest request) {
		isEmptyField("id", request.getId());
		maxFieldLength("id", request.getId(), 2000);
		maxFieldLength("title", request.getTitle(), 2000);
		maxFieldLength("content", request.getContent(), 2000);
	}

	public void validate(String query) {
		if (StringUtils.isBlank(query)) {
			throw new EmptyValueException("query");
		}
	}

	private void maxFieldLength(String fieldName, String value, int maxLength) {
		if (value.length() > maxLength) {
			throw new MaxValueLengthException(fieldName, maxLength);
		}
	}

	private void isEmptyField(String fieldName, String value) {
		if (StringUtils.isBlank(value)) {
			throw new EmptyValueException(fieldName);
		}
	}

}
