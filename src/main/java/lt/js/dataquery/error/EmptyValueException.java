package lt.js.dataquery.error;

import static lt.js.dataquery.util.Messages.EMPTY_PARAM;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(BAD_REQUEST)
public class EmptyValueException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmptyValueException(String fieldName) {
		super(String.format("%s %s", EMPTY_PARAM, fieldName));
	}

}