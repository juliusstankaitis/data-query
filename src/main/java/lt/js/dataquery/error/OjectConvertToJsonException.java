package lt.js.dataquery.error;

import static lt.js.dataquery.util.Messages.OBJECT_TO_JSON_ERROR;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(SERVICE_UNAVAILABLE)
public class OjectConvertToJsonException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public OjectConvertToJsonException(String message) {
		super(String.format("%s %s", OBJECT_TO_JSON_ERROR, message));
	}

}
