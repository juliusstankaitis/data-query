package lt.js.dataquery.error;

import static lt.js.dataquery.util.Messages.MAX_PARAM_LENGTH;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(BAD_REQUEST)
public class MaxValueLengthException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MaxValueLengthException(String fieldName, int length) {
		super(String.format("%s%s%s", MAX_PARAM_LENGTH, fieldName, length));
	}

}
