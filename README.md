Data-query application

This program uses REST API. Suggestions to test code:
1. use program PostMan, import file data-query.postman_collection.json 
2. run integration test DataQueryControllerIntegrationTest.java


technology stack:
java 
maven
junit
spring Boot
jpa
h2
lombok
postMan




